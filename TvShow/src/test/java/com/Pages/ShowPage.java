package com.Pages;

import com.Utils.Util;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.reports.adaptors.xunit.model.TestError;

public class ShowPage extends PageObject {
	Util _Util;
	@FindBy(name = "search")
	private WebElementFacade inputSearch;
	
	@FindBy(css = ".btn")
	private WebElementFacade btnSearch;
		
	@Step
	public void Search_box(String value) throws Exception{
		
		inputSearch.sendKeys(value);
		_Util.ThreadSleep(2);
	}

	@Step
	public void Press_button() throws Exception {
		btnSearch.click();
		_Util.ThreadSleep(2);
		
	}

	@Step
	public void Search_box_validation() throws TestError {
		inputSearch.clear();
		if(!inputSearch.getTextValue().isEmpty())
		{
			throw new TestError("Test Failed: input for search is not empty");
		}
	}
}
