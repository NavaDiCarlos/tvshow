package com.Pages;

import com.Utils.Util;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;


public class SearchPage extends PageObject {
	Util _Util;

	@FindBy(xpath = "(//a[contains(text(),'URL')])[2]")
	private WebElementFacade urlTheBatman;
	
	@FindBy(css = ".btn")
	private WebElementFacade btnBack;

	@Step
	public void Navigate_to_the_url() throws Exception {
		for (int x = 0; x <= 2; x++) {
			_Util.Scroll("0", "550");
		}
		_Util.ThreadSleep(1);
		urlTheBatman.click();
		_Util.ThreadSleep(2);

	}

	@Step
	public void Navigate_back()throws Exception{
		getDriver().navigate().back();
		_Util.ThreadSleep(2);
		
	}

	@Step
	public void Change_ccs(String color, String element) throws Exception {
	
	_Util.Change_Css(color, element);
	_Util.ThreadSleep(3);
		
	}

	@Step
	public void Press_button() {
		btnBack.click();
		_Util.ThreadSleep(2);
		
	}

	

}
