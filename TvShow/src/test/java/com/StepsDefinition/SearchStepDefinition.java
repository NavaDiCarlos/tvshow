package com.StepsDefinition;

import com.Pages.SearchPage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;


public class SearchStepDefinition  {

	@Steps
	SearchPage _SearchPage;
	
	
	@And("^Navigate to the url that is show in second card of results$")
	  public void Navigate_to_the_url_that_is_show_in_second_card_of_results() throws Exception{
		  try {
			  _SearchPage.Navigate_to_the_url();
		  }catch(Exception ex) 
		  {
			  
		  }
	  }
	
	  @And("^Navigate back using browser features$")
	  public void Navigate_back_using_browser_features() throws Exception{
		  try {
			  _SearchPage.Navigate_back();
		  }catch(Exception ex) 
		  {
			  
		  }
	  }
	  
	  @Then("^Change css background color to (.*) to card with title (.*)$")
	  public void Change_css_background_color_to_to_card_with_title(String color, String title) throws Exception{
		  try {
			  _SearchPage.Change_ccs(color, title);
		  }catch(Exception ex) 
		  {
			  
		  } 
	  }
	  @And("^Press back button$")
	  public void Press_back_button() throws Exception{
		  try {
			  _SearchPage.Press_button();
		  }catch(Exception ex) 
		  {
			  
		  } 
	  }
	  
	  
	  
	  
	  
	  
	
}
