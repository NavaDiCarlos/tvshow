package com.StepsDefinition;

import cucumber.api.java.Before;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:3000/shows")
public class Hooks extends PageObject {

static Hooks _hook = new Hooks();
	@Before
	public static void BeforeScenario() {
		_hook.open();
	}
	
}
