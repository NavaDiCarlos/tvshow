package com.StepsDefinition;

import com.Pages.ShowPage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.reports.adaptors.xunit.model.TestError;


public class ShowsStepDefinition {
	@Steps
	ShowPage _ShowPage;
	
		
	@Then("^Enter a text in search box with text (.*)$")
	public void Enter_a_text_in_search_box_with_text(String value) throws Exception {
		try {
			
			_ShowPage.Search_box(value);
		} catch (Exception ex) {
			System.out.println(ex);
		}
	} 
	
	  @When("^Press button search$")
	  public void Press_button_search() throws Exception{
		  try {
			  _ShowPage.Press_button();
		  }catch(Exception ex) 
		  {
			  
		  }
	  }
	  
	  @Then("^Make sure that input for search is empty$")
		public void Make_sure_that_input_for_search_is_empty() throws Exception, TestError {
			try {
				
				_ShowPage.Search_box_validation();
			} catch (Exception ex) {
				System.out.println(ex);
			}
		} 
	
}
