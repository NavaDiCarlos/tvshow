package com.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.pages.PageObject;

public class Util extends PageObject {

	private JavascriptExecutor _js;

	private String[] paths = { "//div[2]/div/div/div" };

	public void ThreadSleep(int segundos) {
		try {
			Thread.sleep(segundos * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void Scroll(String coordenadaX, String cordenadaY) {
		_js = (JavascriptExecutor) getDriver();
		_js.executeScript("window.scroll(" + coordenadaX + "," + cordenadaY + ")");
	}

	public void Change_Css(String color, String Element) {
		String findBy = "";
		_js = (JavascriptExecutor) getDriver();
		switch (Element) {
		case "Batman Unlimited":
			findBy = paths[0];
			break;
		default:
			break;
		}
		WebElement webElement = getDriver().findElement(By.xpath(findBy));
		_js.executeScript("arguments[0].setAttribute('style', 'background-Color: #" + color + "')", webElement);
	}
	
	public void MensajeAlerta(String mensaje) {
		_js = (JavascriptExecutor) getDriver();
		_js.executeScript("alert ('" + mensaje + "')");
		ThreadSleep(3);
	}

}
