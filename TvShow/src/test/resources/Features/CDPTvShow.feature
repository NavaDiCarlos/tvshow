#Author: navadicarlos@gmail.com

Feature: TvShow 
  
  @tag1
  Scenario Outline: TvShow search Title of your scenario
    Then Enter a text in search box with text <value>
    When Press button search
    Then Navigate to the url that is show in second card of results
    And Navigate back using browser features
    Then Change css background color to <color>  to card with title <title>
    And Press back button
    And Make sure that input for search is empty

     Examples: 
      | value  | color     | title 					|
      | batman | 4a148c    |Batman Unlimited|
      
